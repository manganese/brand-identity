# Manganese - Brand identity

## Installation

Reference the `brand.scss` file to pull in the relevant styles.

## Usage

See the `index.html` file in this directory for examples of how to use the various styles and components.

### Building

To rebuild the styles after making changes:

```bash
gulp
```