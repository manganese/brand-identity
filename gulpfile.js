// Paths
const OUTPUT_PATH = "./.static/";

// Gulp
const gulp = require("gulp");
const {
  src,
  dest,
  series,
  parallel
} = gulp;

// Clean
var del = require("del");

function clean() {
  return del(join(OUTPUT_PATH, "*"));
}

// Styles
const sass = require("gulp-sass");

function buildBrandStyles() {
  return gulp
    .src("./brand.scss")
    .pipe(
      sass({
        includePaths: [
          OUTPUT_PATH
        ]
      })
      .on("error", sass.logError)
    )
    .pipe(dest(OUTPUT_PATH));
}

function buildDocumentationStyles() {
  return gulp
    .src("./index.scss")
    .pipe(
      sass()
      .on("error", sass.logError)
    )
    .pipe(dest("."));
}

// Default task
exports.default = parallel(
  buildBrandStyles,
  buildDocumentationStyles
);